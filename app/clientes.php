<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class clientes extends Model {

	//
	protected $table = 'clientes';

	public $timestamps = false;
	protected $fillable = ['nombre_clie', 'descripcion', 'img','titulo','columnas'];
}
