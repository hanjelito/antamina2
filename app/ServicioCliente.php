<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicioCliente extends Model {

	protected $table = 'servicio_cliente';
	public $timestamps = false;
	protected $fillable = ['servicio_id', 'clientes_id'];

}
