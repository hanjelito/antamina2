<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Testexamen extends Model {
	protected $table = 'test_examen';

    protected $fillable = [
        'id_usuario',
        'id_examen',
        'puntaje',
        'fecha_ini',
        'fecha_fin'
    ];
    //public $timestamps = false;
}
