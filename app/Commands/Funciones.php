<?php

namespace App\Commands;
use DB;
class Funciones extends Command
{
	static public function creaExamen($id)
	{
		$examendt = DB::table('dat_preguntas')
			->where('id_examen','=',$id)
			->orderBy(DB::raw('RAND()'))
			->take('20')
            ->get();
        return $examendt;
	}
	static public function creaExamenEdit($id)
	{
		$examendt = DB::table('dat_preguntas')
			->where('id_examen','=',$id)
            ->get();
        return $examendt;
	}
	static public function creaPreguntas($id)
	{
		$examendt = DB::table('dat_alternativas')
			->where('id_pregunta','=',$id)
            ->get();
        return $examendt;
	}
	static public function InverFecha($fecha)
	{
		$nvafecha=explode('-', $fecha);
		return $nvafecha[2].'-'.$nvafecha[1].'-'.$nvafecha[0];
	}
	static public function NotafechaMaxima($fecha)
	{
		$nfecha = explode(',', $fecha);
		if(sizeof($nfecha)>1){
			$mayor = 0;
			$fechanueva = '';
			for ($i=0; $i < sizeof($nfecha); $i++) {
				$fechat = explode('--', $nfecha[$i]);
				if ($fechat[0]>$mayor) {
					$mayor=$fechat[0];
					$fechanueva=$fechat[1];
				}
			}
			$mysdatos[0]=$mayor;
			$mysdatos[1]=$fechanueva;
			return $mysdatos;
		}else{
			$mysdatos = explode('--',$fecha);
			return $mysdatos;
		}

	}
	static public function NotafechaMaxima2($iduser)
	{
		$examendt = DB::table('test_examen')
			->where('id_usuario','=',$iduser)
            ->get();
        $nota=-1;
        foreach ($examendt as $key) {
        	if($key->puntaje>$nota){
        		$fecha=explode(' ',$key->created_at);
        		$nota=$key->puntaje.'-=-'.Funciones::InverFecha($fecha[0]);
        	}
        }
		return $nota;
	}
}
