<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Examen extends Model {
	protected $table = 'dat_examen';
    protected $fillable = [
        'titulo',
        'vigencia',
        'estado',
        'descripcion',
        'nivel'
    ];
    public $timestamps = false;
}
