<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Alternativas extends Model {
	protected $table = 'dat_alternativas';
    protected $fillable = [
        'respuesta',
        'id_pregunta',
        'verasidad'
    ];
    public $timestamps = false;
}
