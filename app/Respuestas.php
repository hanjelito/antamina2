<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuestas extends Model {
	protected $table = 'test_respuestas';
    protected $fillable = [
        'id_examen',
        'id_pregunta',
        'id_testexamen',
        'id_alternativa',
        'id_usuario',
        'valides',
        'puntaje'
    ];
}
