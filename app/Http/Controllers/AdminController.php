<?php namespace App\Http\Controllers;

use App\Commands\UserCommand;
use App\Commands\Funciones;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Examenes;
use App\User;
use Input;
use App\Examen;
use App\Preguntas;
use App\Respuestas;
use App\Alternativas;
use App\Testexamen;

use Validator;
use DB;
use Auth;
use Hash;

class AdminController extends Controller{

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function getIndex()
	{
		/*$sliders = sliders::all();
		$menus = menu::all();
		$servicioc = DB::table('servicio_cliente')
		  	->join('clientes', 'servicio_cliente.clientes_id', '=', 'clientes.id')
            ->join('servicio', 'servicio_cliente.servicio_id', '=', 'servicio.id')
            ->select('servicio_cliente.id', 'clientes.nombre_clie', 'servicio.nombre_servi')
            ->get();*/

		return view('admin.index');
	}


	public function getIndicaciones()
	{
		/*$sliders = sliders::all();
		$menus = menu::all();
		$servicioc = DB::table('servicio_cliente')
		  	->join('clientes', 'servicio_cliente.clientes_id', '=', 'clientes.id')
            ->join('servicio', 'servicio_cliente.servicio_id', '=', 'servicio.id')
            ->select('servicio_cliente.id', 'clientes.nombre_clie', 'servicio.nombre_servi')
            ->get();*/

		return view('admin.indicaciones');
	}

	public function getExamenes()
	{
		/*$Examen = Examenes::all();*/
		$examendt = DB::table('dat_examen')
            ->get();

		return view('admin.examenes')->with('dtexamen',$examendt);
	}
	public function postSaveexamen(Request $request)
	{
		$resultado= Examen::create([
			'titulo' => $request->input('nameexamen')
		]);
		return redirect('editexamen/'.$resultado->id);
	}
	public function getEditexamen($id)
	{
		$examendt = DB::table('dat_examen')
			->where('id','=',$id)
            ->get();





		return view('admin.creaexamen')->with('dtexamen',$examendt);
	}
	public function getPost()
	{

		return view('admin.examenes');
	}
	public function postGenpregunta($id,Request $request)
	{

		$prgtas = Input::get('prgtas');
		$altva=$request->input('veridico');
			$resultado= Preguntas::create([
				'pregunta' => $request->input('ttlprgta'),
				'contenido' => $request->input('ctndoprgta'),
				'puntaje' => $request->input('pntg'),
				'id_examen' => $id
			]);
		for ($i=0; $i < sizeof($prgtas); $i++)
		{
			$alternativa= Alternativas::create([
				'respuesta' => $prgtas[$i],
				'verasidad' => $altva[$i],
				'id_pregunta' => $resultado->id
			]);
		}
		return redirect()->back();
	}
	public function getNuevousuario()
	{
        $niveles = [
        	1 => 'Usuario',
        	2 => 'Administrador',
        ];

		return view('admin.nuevousuario')->with('niveles',$niveles);
	}

	public function postSavenuevousuario(Request $request)
	{
		//['name','last_name','password','codigo','acceso','genero','puesto','area','gerencia','categoria','nivel'];
		$nivelu = 1;
		if($request->input('nivel') == 0){
			$nivelu = 1;
		}
		else{
			$nivelu =  $request->input('nivel');
		}

		$codigoi = (int) $request->input('codigo');
		$pass = Hash::make('antamina');
		//$pass = md5('antamina');

		$resultado= User::create([
			'name' => $request->input('name'),
			'last_name' => $request->input('last_name'),
			'codigo' => $codigoi,
			'email' => $request->input('email'),
			'acceso' => 0,
			'password' => $pass,
			'genero' => $request->input('genero'),
			'puesto' => $request->input('puesto'),
			'area' => $request->input('area'),
			'gerencia' => $request->input('gerencia'),
			'categoria' => $request->input('categoria'),
			'nivel' => $nivelu
		]);

	 	$niveles = [
        	1 => 'Usuario',
        	2 => 'Administrador',
        ];

		return view('admin.nuevousuario')->with('niveles',$niveles);
	}

	public function getActualizarpass()
	{
		$revisar = 0;
		return view('admin.actualizarpass')->with('revisar',$revisar);
	}

	public function postActualizarpass($codigo, Request $request)
	{
		if( $request->input('nuevo') == "" || $request->input('repetir') == "" ){
			$revisar = 1;
			return view('admin.actualizarpass')->with('revisar',$revisar);
		}
		if( $request->input('nuevo') != $request->input('repetir') ){
			$revisar = 2;
			return view('admin.actualizarpass')->with('revisar',$revisar);
		}
		$revisar = 3;
		$pass = Hash::make($request->input('nuevo'));
		DB::table('users')
            ->where('codigo', $codigo)
            ->update(['password' => $pass]);
		return view('admin.actualizarpass')->with('revisar',$revisar);
	}

	public function getTest()
	{
		$su = 0;
		$max=0;

		$examendt = DB::table('dat_examen')
			->select('*',DB::raw($su.' as suma'))
            ->get();



        for ($i=0; $i < sizeof($examendt) ; $i++) {

        	 $max  = DB::table('test_examen')
	        	->where('id_examen','=',$examendt[$i]->id)
	        	->where('id_usuario','=',Auth::user()->id)
	        	->max('puntaje');

	       	$fecha = DB::table('test_examen')
	       		->where('id_examen','=',$examendt[$i]->id)
	        	->where('id_usuario','=',Auth::user()->id)
	        	->where('puntaje','=',$max)
	        	->get();


        	$examendt[$i]->suma=$max;
        	if(sizeof($fecha)>0){
        		$examendt[$i]->vigencia = $fecha[0]->created_at;
        	}
        }

       // $examendt[0]->suma = 10;



		return view('admin.test')->with('dtexamen',$examendt);


	}
	public function getExamen($id)
	{

		$examendt = DB::table('dat_examen')
			->where('id','=',$id)
            ->get();

        $alternativa = Testexamen::create([
						'id_usuario' => Auth::user()->id,
						'id_examen' => $id
					]);
      	$idtestexam = $alternativa->id;

        //return Auth::user()->id." ".$id." "."1"." ".$date." ".$date;


		return view('admin.evaluacion')->with('dtexamen',$examendt)->with('idtestexam',$idtestexam);
	}
	public function getRespuestas($id)
	{
		$examen = DB::table('test_examen')
			->join('dat_examen', 'dat_examen.id', '=', 'test_examen.id_examen')
			->where('test_examen.id','=',$id)
			->where('test_examen.id_usuario','=',Auth::user()->id)
	        ->get();
		    $preguntas = DB::table('test_respuestas')
	    	->join('dat_preguntas', 'dat_preguntas.id', '=', 'test_respuestas.id_pregunta')
			->where('test_respuestas.id_usuario','=',Auth::user()->id)
			->where('test_respuestas.id_testexamen','=',$id)
	        ->get();
		return view('admin.verrespuestas')->with('examen',$examen)->with('preguntas',$preguntas);
	}
	public function postCreatest($id,Request $request)
	{
		$myrspts = $request->input();
		$condiciones=0;

		$idtestexam = $request->input('idtest');

		foreach($myrspts as $key => $value)
		{

			if ($condiciones>1) {
				$alternativaDB = DB::table('dat_alternativas')
					->select('*','dat_alternativas.id as ides','dat_preguntas.id as idp')
					->join('dat_preguntas', 'dat_preguntas.id', '=', 'dat_alternativas.id_pregunta')
					->where('dat_alternativas.id','=',$request->input($key))
		            ->get();


				foreach ($alternativaDB as $key) {

					$alternativa= Respuestas::create([
						'id_examen' => $key->id_examen,
						'id_pregunta' => $key->idp,
						'id_testexamen' => $idtestexam,
						'id_alternativa' => $key->ides,
						'puntaje' => $key->puntaje,
						'valides' => $key->verasidad,
						'id_usuario' => Auth::user()->id
					]);
				}
			}
		  	$condiciones++;
		}

		$test = DB::table('test_respuestas')
	 		->where('test_respuestas.id_testexamen', '=',$idtestexam)
	 		->where('test_respuestas.valides','=',1)
	 		->sum('test_respuestas.puntaje');

		$testexan= Testexamen::find($idtestexam);
		$testexan->puntaje=$test;
		$testexan->save();

		return redirect('intentos/'.$id);
	}
	public function getIntentos($id)
	{
		/*$Examen = Examenes::all();*/
		$intentos = DB::table('test_respuestas')
			->select('*',DB::raw('sum(puntaje) as suma'))
			->where('valides','=',1)
			->where('id_examen','=',$id)
			->groupBy('created_at')
            ->get();

        $result = DB::table('test_examen')
        	->where('id_examen','=',$id)
        	->where('id_usuario','=',Auth::user()->id)
        	->get();

		return view('admin.intentos')->with('intentos',$intentos)->with('result',$result);
	}
	public function getCertificados($id)
	{
		$user = DB::table('test_examen')
			->select('*',
				DB::raw('group_concat(test_examen.puntaje,"--",test_examen.created_at) AS fech')
			)
			->join('users','users.id','=','test_examen.id_usuario')
			->where('test_examen.id_examen','=', $id)
			->where('test_examen.id_usuario','=', Auth::user()->id)
			->groupBy('test_examen.id_usuario')
      ->get();
		return view('admin.certificado')->with('user',$user);
	}
}

?>
