<?php namespace App\Http\Controllers;

use App\Commands\UserCommand;
use App\Commands\Funciones;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Input;
use Validator;
use DB;
use Auth;
use App\User;
use Hash;

class AdmController extends Controller{

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function getIndex()
	{

		$user = DB::table('users')
			->where('acceso','=', 1)
            ->get();
		return view('super.usuarios')->with('user',$user);
	}
	public function getUsuarios()
	{

		$user = DB::table('users')
            ->get();
		return view('super.index')->with('user',$user);
	}
	public function postQuitar(Request $request)
	{

		$user = User::find($request->input('ides'));
		$user->delete();
		return redirect()->back();
	}
	public function getUsuariosno()
	{

		$user = DB::table('users')
			->where('acceso','=', 0)
            ->get();
		return view('super.noingresaron')->with('user',$user);
	}
	public function getSiexamen()
	{

		$user = DB::table('test_examen')
			->join('users','users.id','=','test_examen.id_usuario')
			->groupBy('test_examen.id_usuario')
            ->get();
		return view('super.siexamen')->with('user',$user);
	}
	public function getNoexamen()
	{

		$user = DB::table('test_examen')
			->rightjoin('users','users.id','=','test_examen.id_usuario')
			->where('test_examen.id_usuario','=', NULL)
            ->get();

		return view('super.noexamen')->with('user',$user);
	}
	public function getEditaruser($cod)
	{
		$usuario = DB::table('users')
			->where('codigo','=',$cod)
			->select('*')
			->get();

		$niveles = [
        	1 => 'Usuario',
        	2 => 'Administrador',
        ];

		return view('super.editaruser')->with('usuario',$usuario[0])->with('niveles',$niveles);
	}

	public function postEditaruser(Request $request)
	{

		if($request->input('nivel') != 0){
			DB::table('users')
				->where('codigo', $request->input('codigo'))
	            ->update([ 'nivel' =>  $request->input('nivel') ]);
		}

		if($request->input('password')!="")
		{
			$pass = Hash::make($request->input('password'));
			DB::table('users')
				->where('codigo', $request->input('codigo'))
	            ->update([ 'password' => $pass ]);
		}

		DB::table('users')
			->where('codigo', $request->input('codigo'))
            ->update([
            	'name' => $request->input('name'),
            	'last_name' => $request->input('last_name'),
            	'genero' => $request->input('genero'),
            	'email' => $request->input('email'),
            	'puesto' => $request->input('puesto'),
            	'area' => $request->input('area'),
            	'gerencia' => $request->input('gerencia'),
            	'categoria' => $request->input('categoria')
            	]);

		$niveles = [
        	1 => 'Usuario',
        	2 => 'Administrador',
        ];

       	$usuario = DB::table('users')
			->where('codigo','=',$request->input('codigo'))
			->select('*')
			->get();


		return view('super.editaruser')->with('usuario',$usuario[0])->with('niveles',$niveles);
	}

}

?>
