<?php namespace App\Http\Controllers;

use App\Commands\UserCommand;
use App\User;
use App\sliders;
use App\clientes;
use App\Servicio;
use DB;

class FrontController extends Controller{

	public function __contruct()
	{

	}

	public function getIndex()
	{
		$users = User::all();
		$sliders = sliders::all();
		$clientes = clientes::all();
		$servicios = Servicio::all();

		$clie_serv = DB::table('servicio_cliente')
			->join('servicio','.servicio_cliente.servicio_id','=','servicio.id')
			->select('servicio_cliente.id as id_sc','servicio_cliente.servicio_id','servicio_cliente.clientes_id as clientes_id','servicio.nombre_servi')
			->get();
		
		$cc = [];
		foreach ($clientes as $clie) {
			$array_servicios = '';
			foreach ($clie_serv as $cs) {
				if ($cs->clientes_id == $clie->id) {
					$array_servicios = $array_servicios . $cs->nombre_servi .' / ';
				}
			}
			$array_servicios = substr($array_servicios, 0, -2);
			$cc = array_add($cc,$clie->id,[$clie->id,$clie->nombre_clie,$clie->descripcion,$clie->img,$clie->titulo,$clie->columnas, $array_servicios]);
		}

		return view('front.index')->with('users', $users)->with('banners',$sliders)->with('clientes',$cc)->with('servicios',$servicios)->with('clie_serv',$clie_serv);
	}

}

?>