<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Preguntas extends Model {
	protected $table = 'dat_preguntas';
    protected $fillable = [
        'pregunta',
        'contenido',
        'id_examen',
        'puntaje'
    ];
    public $timestamps = false;
}
