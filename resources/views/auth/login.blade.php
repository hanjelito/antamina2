@extends('app')

@section('content')
<style type="text/css">

	.logoant img
	{
		width: 270px;
	}
	.contlog
	{
		background-color: #5C5B60;
	}
	.panel-default > .panel-heading {
	  background-color: transparent;
	  border: none;
	}
	.contenedor
	{
		border-radius: 25px;
		text-align: center;
		width: 345px;
		margin: 10% auto;
		-webkit-box-shadow: 9px 10px 12px -4px rgba(0,0,0,0.75);
		-moz-box-shadow: 9px 10px 12px -4px rgba(0,0,0,0.75);
		box-shadow: 9px 10px 12px -4px rgba(0,0,0,0.75);
	}
	.panel
	{
		border-radius: 5px;
		padding: 15px;
	}
	.panel-body {
	  padding: 15px;
	  background: #DFDFE1;
	  width: 300px;
	  margin: 0 auto;
	  border-radius: 5px;
	}
	.contenedor h4{
	  font-size: 15px;
	  font-weight: bold;
	  color: #fff;
	}
	.contenedor .alerta{
	  font-weight: bold;
	  color: #fff;
	}
	.panel-default {
	  border:0;
	}
	.form-horizontal .radio, .form-horizontal .checkbox {
	  min-height: 27px;
	  text-align: left;
	  padding: 0 20px;
	}
	.green
	{
		background-color: #D78C20;
		width: 270px;
	}
	.foot
	{
		color: #fff;
		padding: 10px 0 0 0;
	}
	.btn_clr{
		color: #fff;
		font-weight: bold;
	}
	#input-user
	{
		background-image: url('../images/user.jpg');
		background-repeat: no-repeat;
		background-size: 25px;
		background-position: right;

	}
	#input-pass
	{
		background-image: url('../images/pass.jpg');
		background-repeat: no-repeat;
		background-size: 21px;
		background-position: right;

	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="contenedor">
			<div class="panel panel-default contlog">
				<div class="panel-heading logoant contlog">
					<img src="../../images/logo-familia-antamina.png" alt="">
					<div>
						<h4>PLATAFORMA DE APRENDIZAJE</h4>
						@if (count($errors) > 0)
						<div class="alerta">
							<strong>El código de usuario o la contraseña no son correctos</strong><br><br>
						</div>
					@endif
					</div>
				</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<div class="col-md-12">
								<input id="input-user" class="form-control" name="codigo" placeholder="4 dígitos de vendor" value="{{ old('email') }}" maxlength="4">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<input id="input-pass" type="password" class="form-control" placeholder="Contraseña" name="password" >
							</div>
						</div>

						<div class="form-group">
							<div class="checkbox">
								<label>
									<input id="nopassw" type="checkbox" name="nouser"> Aun no tengo contraseña
								</label>
							</div>
						</div>

						<div class="form-group">
								<button type="submit" class="btn green btn_clr">ENTRAR</button>
						</div>
					</form>
				</div>
				<div class="form-group foot">
					Consultas a: soporte@familiaantamina.com
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
