<nav class="navbar navbar-default">
  <div class="container" >
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><img src="../../images/logotop.jpg" alt=""></li>
       <!-- <li><a href="{{ url('/admin/examenes') }}">Crear Examene</a></li>-->
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hola: {{ Auth::user()->name }} <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ url('/admin/test') }}">Tabla de evaluaciones</a></li>
            @if (Auth::user()->nivel == 2)
              <li><a href="{{ url('/admin/nuevousuario') }}">Crear usuario</a></li>
              <li><a href="{{ url('/superadmin') }}">Usuarios que ingresaron al Sistema</a></li>
              <li><a href="{{ url('/superadmin/usuariosno') }}">Usuarios que no ingresaron al Sistema</a></li>
              <li><a href="{{ url('/superadmin/siexamen') }}">Usuarios dieron Exámen</a></li>
              <li><a href="{{ url('/superadmin/noexamen') }}">Usuarios que no dieron Exámen</a></li>
              <li><a href="{{ url('/superadmin/usuarios') }}">Todos Los Usuarios</a></li>
            @endif
            <li class="divider"></li>
            <li><a href="{{ url('/auth/logout') }}">Salir</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="page-content">
  <div class="container" >
      <div class="row cont-pg titulo">
        <div style="text-align:">
        <h3 >PLATAFORMA DE CAPACITACION FAMILIA ANTAMINA 2015</h3>
      </div>
    </div>
  </div>
</div>
<div class="page-content">
  <div class="container">
    <div class="row cont-pg margen">
      <img src="../../images/banner.jpg" class="img-responsive">
    </div>
  </div>
</div>
  @yield('content')