@include('super.include.cabecera')
@include('super.include.menu')
	<div class="page-content">
	  <div class="container" >
	    <div style="background:#fff;text-align: center;">
	      <div class="row cont-pg">
			  <div class="col-md-12 conder">
          		<h2 style="color:#E99130">Todos los usuarios</h2>
		          <div class="portlet light">
		            <div class="portlet-body">
		              <div class="panel">
		                <table  id="tabla-actual"  class="table table-bordered table-striped ">
		                <thead>
		                <tr>
		                	<th>Código</th>
		                  	<th>Nombre y Apellido</th>
		                  	<th>Genero</th>
		                  	<th>Categoría</th>
		                  	<th>Puesto</th>
		                  	<th>Área</th>
		                  	<th>Gerencia</th>
		                  	<th></th>
		                </tr>
		                </thead>
		                <tbody>
		                	@foreach ($user as $useDB)
		                  	<tr>
		                  	<td><a href="{!! url('superadmin/editaruser/'.$useDB->codigo) !!}" title="">{{ $useDB->codigo }}</a></td>
												<td>{{ $useDB->name }} {{ $useDB->second_name }} {{ $useDB->last_name }} {{ $useDB->last_namem }}</td>
			                  <td>{{ $useDB->genero }}</td>
			                  <td>{{ $useDB->categoria }}</td>
			                  <td>{{ $useDB->puesto }}</td>
			                  <td>{{ $useDB->area }}</td>
			                  <td>{{ $useDB->gerencia }}</td>
			                  <td>
			                  	{!! Form::open(array('url' => 'superadmin/quitar')) !!}
			                  	<input type="hidden" name="ides" value="{{ $useDB->id }}">
			                  	<button type="submit" class="btn" style="background:#E99130;color:white;">Eliminar</button>
			                  	{!! Form::close() !!}
			                  </td>
		                	</tr>
		                	@endforeach
		                </tbody>
		                </table>
		              </div>
		            </div>
		          </div>
		        </div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
</div>
@include('super.include.footer')
