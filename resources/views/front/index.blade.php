@include('front.includes.cabecera')

<div class="row">
	<div id="body-principal" class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
		<div id="header-menu" style="background:white;" >
			<div class="row">
				<div class="col-md-offset-1 col-lg-offset-1 col-xs-offset-1 col-sm-offset-1 col-md-1 col-lg-1 col-xs-1 col-sm-1">
					<span class="header-menu">overbrand</span>
				</div>
				<div class="col-md-offset-9 col-lg-offset-9 col-xs-offset-8 col-sm-offset-8 col-md-1 col-lg-1 col-xs-1 col-sm-1">
					<a class="btn-menu" type="button">
						<span class="glyphicon glyphicon-menu-hamburger header-menu" aria-hidden="true"></span>
					</a>        
				</div>

			</div>
		</div>
		<div id="container-header">
			<div id="banner" align="center" >
				<!--<div style="position:relative;"><img class="img-principal img-responsive" src="{{ asset('images/b4.png') }}" alt="#"/><div class="col-md-offset-3 text-hola texto-emergente"><p>HOLA MUNDO</p></div></div>-->
				@foreach ($banners as $slid)
				<div style="position:relative;"><img class="img-principal img-responsive container" src="{{ 'images/' . $slid->imagen }}" alt="#"/>
					<div class="col-md-offset-2 col-lg-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-1 col-lg-1 col-xs-1 col-sm-1 text-banner-left texto-emergente">
						<p>{{ $slid->nombre }}</p>
					</div>
					<div class="col-md-offset-9 col-lg-offset-9 col-xs-offset-8 col-sm-offset-8 col-md-1 col-lg-1 col-xs-1 col-sm-1 text-banner-rigth texto-emergente">
						<p>{{ $slid->titulo }}</p>
					</div>
				</div>
				@endforeach

			</div>
		</div>

		<div class="parrafo-header" align="center">
			<section  align="center" class="parrafo-header-texto">
				<h1>A Few Words</h1>
				<p style="color:#01DF74">ABOUT KOMMIGRAPHICS</p>
				<p>Kommigraphics Design Studio is an award-winning communication agency, based in Athens – Greece.
				We specialise in effective branding and design, digital print solutions paired with integrated marketing communication strategies and implementation, across a wide range of markets and media.
				We design, evolve and revitalize brands across all platforms and media. Our aim is to consistently produce meaningful results for our clients through successful design.</p>
				<span id="parrafo-cerrar" style="font-weight: bold; ">Ocultar</span>
			</section>
		</div>
		<br>
		<hr style="float: absolute; width:90%;">

		<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 col-xs-10 col-sm-10 text-servicios container">
			<div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
				Comunicacion interna
			</div>
			<div class="col-xs-offset-8 col-sm-offset-8 col-md-offset-8 col-lg-offset-8 col-md-2 col-lg-2 col-xs-2 col-sm-2">
				Filtro
			</div>
			
		</div>

		<br></br>
		<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 col-xs-10 col-sm-10 text-servicios container" style="background:#424242; color:white; padding:2%;">
			<div class="row">
				<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
					<h3>Tipo</h3>
					@foreach ($servicios as $servi)
						<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
							<p onclick="filtrar({{$servi->id}})" class="servicios" id="3"> {{$servi->nombre_servi}}</p>
						</div>
					@endforeach					
				</div>
				<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4" style="border-right: 1px solid white; border-left: 1px solid white;">
					<h3>Servicio</h3>					
					@foreach ($servicios as $servi)
						<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
							<p>{{$servi->nombre_servi}}</p>
						</div>
					@endforeach					
				</div>
				<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
					<h3>Temática</h3>
					@foreach ($servicios as $servi)
						<div class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
							<p>{{$servi->nombre_servi}}</p>
						</div>
					@endforeach					
					
				</div>
			</div>
		</div>
		<hr style="float: absolute; width:50%;color:white">
		<div id="contenedor" class="js-masonry">    

			<!--<div class="grid-sizer" >
				<article>
					<img src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRdjJW2_SJ9b_S0h-xC84161y0Yl_kz9JvrRBlRK8S3qIAFSzs6KA" alt="#">
				</article>
			</div>-->
			@foreach ($clientes as $clie)			
			@if ($clie[5] === 1)
			<div class="item imagenclie uno" id="{{$clie[0]}}">
				<article> <img src="{{ 'images/' . $clie[3] }}" alt="#">
					<h3><a href="#">{{ $clie[4] }}</a></h3>
					<p>{{$clie[2]}}</p>
					<hr style="float: absolute; width:100%;color:white">
					<span> {{$clie[6]}} </span>
				</article>
			</div>			    
			@elseif ($clie[5] === 2)
			<div class="item doble imagenclie dos" id="3">
				<article> <img src="{{ 'images/' . $clie[3] }}" alt="#">
					<h3><a href="#">{{ $clie[4] }}</a></h3>
					<p>{{$clie[2]}}</p>
					<hr style="float: absolute; width:100%;color:white">
					<span> {{$clie[6]}} </span>
				</article>
			</div>
			@else
			<div class="item triple" id="{{$clie[0]}}">
				<article> <img src="{{ 'images/' . $clie[3] }}" alt="#">
					<h3><a href="#">{{ $clie[4] }}</a></h3>
					<p>{{$clie[2]}}</p>
					<hr style="float: absolute; width:100%;color:white">
					<span> {{$clie[6]}} </span>
				</article>
			</div>
			@endif
			@endforeach
		</div>
	</div>
	
	<!--<div class="background-cont-menu col-sm-offset-11 col-md-2">
		<img id="im2" src="{{ asset('images/background.png') }}" style="position:absolute;" alt="#"/>
	</div>-->
	<div id="body-menu" class="cont-menu col-md-offset-11 col-lg-offset-11 col-xs-offset-11 col-sm-offset-11 col-md-3 col-lg-3 col-xs-3 col-sm-3">
		<br>
		<div class="row">
			<div align="right" class="col-md-offset-8 col-lg-offset-8 col-xs-offset-6 col-sm-offset-6  col-md-1 col-lg-1 col-xs-1 col-sm-1">
				<p><span class="glyphicon glyphicon-remove btn-menu" aria-hidden="true"></span></p>
			</div>
		</div>

		<ul>
			<li><p>INICIO</p></li>
			<li><p>PROYECTOS</p></li>
			<li><p>SERVICIOS</p></li>
			<li><p>CLIENTES</p></li>
			<li><p>CONTACTO</p></li>
		</ul>
		<hr style="float: absolute; color: #FFF; width:70%;">
		<ul>
			<li><p>Nosotros</p></li>
			<li><p>Blog</p></li>
		</ul>
		<hr style="float: absolute; color: #FFF; width:70%;">

		<ul>
			<li><p>Inicia tu proyecto</p></li>
		</ul>
		<hr style="float: relative; color: #FFF; width:70%;">
		<ul>
			<li><p>Síguenos</p></li>
			<li>
				<p>
					<span class="glyphicon glyphicon-globe" aria-hidden="true"></span> .
					<span class="glyphicon glyphicon-globe" aria-hidden="true"></span> .
					<span class="glyphicon glyphicon-globe" aria-hidden="true"></span> .
					<span class="glyphicon glyphicon-globe" aria-hidden="true"></span> .
				</p>
			</li>
			<li><p>Newsletter</p></li>
			<li><p><input type="text" name="nombreContacto" placeholder="Nombre completo" style="width:90%;font-size: 75%;color:#363538;"></p></li>
			<li><p><input type="text" name="emailContacto" placeholder="Email" style="width:70%;font-size: 75%;color:#363538;"><input type="submit" value="ENVIAR" style="height:100%; width:20%;font-size: 45%;font-weight: bold; color:black; backgroud:#E5E7ED;padding-left: 0px;padding-right: 0px;padding-top: 8px;padding-bottom: 4px;"></p></li>
		</ul>
	</div>
</div>

<script>

/*$('.servicios').on('click', function(event) {
	console.log("SI");
	var ides = $(this).attr('id');

  	$('.imagenclie').each(function(index, el) {
  		console.log($(this).attr('id'));
   	if($(this).attr('id')==ides)
   	{
    	$(this).css('display', 'none');
    	$('#contenedor').masonry('bindResize');
    	/*var container = document.querySelector('#contenedor');

    	var container = new Masonry(container, {
			columnWidth: '.grid-sizer',
			itemSelector: '.item',
			"percentPosition": true
		});
   	}
  	});


});*/


</script>

@include('front.includes.footer')