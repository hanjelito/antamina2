@include('admin.includes.cabecera')
@include('admin.includes.menu')
<link href='<?php echo URL::asset('css/nuevouser.css'); ?>' rel='stylesheet' type='text/css'>
<div class="page-content">
	<div class="container" >
		<div class="conder" style="background:#fff;text-align: center;">
			<div class="row cont-pg">
				<div class="col-md-12">
					<div class="portlet light">
						<div style="text-align:right;">
							<a href="{!! url('/') !!}" style="color:#000;font-style: italic;text-decoration: underline;" title="">Volver al menú de contenidos</a>
						</div>
						<div class="row">

							<div class="col-md-offset-3 col-sm-offset-1 col-xs-offset-1 col-xs-10 col-sm-10 col-md-7">
								<div class="text-center" style="color:red">
									@if($revisar == 1)
										<h4 style="color:red" >Debe llenar ambos campos</h4>
									@elseif($revisar == 2)
										<h4 style="color:red" >No coinciden ambas contraseñas</h4>
									@elseif($revisar == 3)
										<h4 style="color:green" >Se actualizó correctamente</h4>
									@endif
								</div>
							{!! Form::open(array('url' => 'admin/actualizarpass/'. Auth::user()->codigo)) !!}
    							<div class="form-group">
    								{!! Form::label('nuevo','Nueva contraseña:') !!}
    								{!! Form::password('nuevo',['class'=> 'form-control','placeholder'=>'Nueva contraseña','id' => 'nuevo'])!!}
    							</div>
                                <div class="form-group">
                                    {!! Form::label('repetir','Repetir contraseña:') !!}
                                    {!! Form::password('repetir',['class'=> 'form-control','placeholder'=>'Repetir contraseña','id' => 'repetir'])!!}
                                </div>
    							<div class="form-actions fluid">
									<div class="row">
										<div class="text-center">
											<button type="submit" class="btn" style="background:#E99130;color:white;">Guardar cambios</button>
										</div>
									</div>
								</div>
							{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
</div>
@include('admin.includes.footer')
