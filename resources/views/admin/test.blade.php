@include('admin.includes.cabecera')
@include('admin.includes.menu')
	<div class="page-content">
	  <div class="container" >
	    <div class="conder" style="background:#fff;text-align: center;">
	      <div class="row cont-pg">

			  <div class="col-md-12" style="height: 300px;">

		          <div class="portlet light">
		          	<div style="text-align:right;">
						<a href="{!! url('/') !!}" style="color:#000;font-style: italic;text-decoration: underline;" title="">Volver al menú de contenidos</a>
					</div>
		            <div class="portlet-body">
		            <h3 class="centrado">MIS EVALUACIONES</h3>
		              <div class="table-responsive">
		                <table class="table table-bordered">
		                <thead>
		                <tr>
		                  <th>DESCRIPCIÓN</th>
		                  <th>FECHA</th>
		                  <th>HORA</th>
		                  <th>CALIFICACIÓN MÁXIMA</th>
		                  <th>REVISIÓN</th>
		                </tr>
		                </thead>
		                <tbody>
		                	@foreach ($dtexamen as $examenes)
		                  	<tr>
			                  <td>{{ $examenes->titulo }}</td>
			                  <td>
			                  <?php $intentofh=explode(' ', $examenes->vigencia) ?>
			                  {{ App\Commands\Funciones::InverFecha($intentofh[0]) }}
			                  </td>
			                  <td>{!!$intentofh[1]!!}</td>
			                  <td>{{ $examenes->suma }}</td>
			                  <td>

			                    	@if ($examenes->estado == 0)
			                    			INHABILITADO
			                    	@elseif ($examenes->estado == 1)
			                    		<a href="<?=URL::to('/admin/intentos/'.$examenes->id); ?>" style="color: ##D78C20;text-decoration: underline;">HABILITADO</a>

			                    	@endif

			                  </td>
		                	</tr>
		                	@endforeach
		                </tbody>
		                </table>
		              </div>
		            </div>
		          </div>
		        </div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
</div>
@include('admin.includes.footer')
