@include('admin.includes.cabecera')
@include('admin.includes.menu')
<div class="page-content">
  <div class="container" >
    <div class="conder">
      <div class="row cont-pg">
        @include('admin.includes.menulateral')
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 contenedorTXT">
          <div class="input-group">
            <div class="contenedoder" id="divtest1" >
              <h3>PRESENTACIÓN</h3>
              El propósito del Programa de Cumplimiento es asegurar que todos los miembros de la familia Antamina están al tanto de las diversas obligaciones
              y exigencias que tenemos para no incurrir en faltas.
              <br>
              <br>
              Las obligaciones del Programa de Cumplimiento son aplicables a todos
              los miembros de la Familia Antamina.  Los puntos más importantes del Programa de Cumplimiento para tener en cuenta son:<br>
              <ul>
                <li>No dar o recibir sobornos, sin importar el monto del mismo. Todo tipo de soborno está prohibido en Antamina.</li>
                <li>No estar involucrado en actos de corrupción.</li>
                <li>No tener Conflictos de Interés que impacten de manera negativa a Antamina.</li>
                <li>No participar en actos de lavado de activos o financiamiento del terrorismo.</li>
                <li>Asegurar que los regalos y atenciones que damos y recibimos están en línea con las Políticas de Antamina.</li>
                <li>Respetar las políticas y procedimientos de Antamina.</li>
              </ul>
              <br><br>
              <div align="center" class="embed-responsive embed-responsive-16by9">
                <video controls preload="none" loop class="embed-responsive-item" poster="../../images/fondovideo.jpg">
                  <source src=videos/antamina1.mp4 type=video/mp4>
                </video>
              </div>
                <br>
                <div align="center" class="embed-responsive embed-responsive-16by9">
                  <video controls preload="none" loop class="embed-responsive-item" poster="../../images/fondovideo.jpg">
                    <source src=videos/antamina2.mp4 type=video/mp4>
                    </video>
                </div>
                <br><br>
              </div>
                <div class="contenedoder" id="divtest2" style="display:none;">
                  <h3>SOBORNO</h3>
                  Soborno se refiere a corromper a alguien con dinero, regalos o algún favor para obtener algo de esta persona. No hay montos mínimos para considerar el soborno, todo pago impropio sin importar el monto o valor se considera un soborno.
                  <br>
                  Es posible distinguir entre dos tipos de soborno:
                  <br>

                  <ul>
                    <li>Cuando se acepta un soborno para cumplir con un acto ilícito.
                      <ul>
                        <li>Ejemplo: si un socio estratégico le paga a un miembro de la Familia Antamina para que sea favorecido en una licitación o concurso.</li>
                      </ul>
                    </li>

                    <li>Cuando se entrega un soborno para obstaculizar o impedir la realización de una acción.
                      <ul>
                        <li>Ejemplo: si un socio estratégico le paga a un miembro de la Familia Antamina para que sea favorecido en una licitación o concurso.</li>
                      </ul>
                    </li>
                  </ul>
                  <br>


                  Es importante señalar que tanto la persona que ofrece así como la que acepta el soborno incurre en un delito. Los sobornos también toman en cuenta el ámbito donde y con quien se desarrollen. Así, por ejemplo, existen los sobornos en el ámbito público que son aquellos en los que participa un funcionario público.<br>
                  <ul>
                    <li>Ejemplo: Cuando un miembro de la familia Antamina paga un soborno a un funcionario público para que se le dé un trámite más ágil a un permiso o solicitud.</li>

                  </ul>
                  <br>
                  Por otro lado, están los sobornos en el ámbito de la esfera privada. Estos son los que se dan entre individuos del ámbito privado para favorecer de manera indebida a una de las partes.
                  Más allá de que el soborno implique un delito, el hecho de corromper a alguien para obtener un beneficio supone una falta ética que, en este sentido, debería ser evitada de sobre manera para no tener que incurrir en un castigo legal.
                  <br><br>
                </div>
                <div class="contenedoder" id="divtest3" style="display:none;">
                  <h3>CONFLICTO DE INTERÉS</h3>
                  Los conflictos de interés son aquellas situaciones en las que un miembro de la Familia Antamina en vez de cumplir con lo debido, guía sus decisiones o actuar en beneficio propio o de un tercero. Los conflictos de intereses están presentes en numerosas decisiones de la vida de profesionales, directivos y empleados, así como de las empresas y organizaciones.
                  <br>
                  Ejemplos:

                  <ul>
                    <li>Si una persona es un miembro de la Familia Antamina y, al mismo tiempo, es dueño de una empresa proveedora de Antamina.
                      <ul>
                        <li>En algún momento, se tendrán que tomar decisiones en relación a los proveedores, y las siguientes preguntas seguramente surgirán: ¿qué se privilegiará: a Antamina o la empresa de la que la persona es dueño?</li>
                      </ul>
                    </li>


                  </ul>
                  <br>
                  Lo que pide el Programa de Cumplimiento es que actuemos con criterio preventivo, y se declare ante nuestro supervisor y al Área de Cumplimiento que una situación puede presentarnos un potencial conflicto de intereses.
                  Factores que implican un posible conflicto de interés
                  <br>
                  Una manera de determinar si hay o no un posible conflicto de interés es evaluar si hay algo que se debería informar al supervisor y a Cumplimiento

                  <ul>
                    <li>¿Existe una relación entre la persona que ingresa o la empresa que se contrata con un miembro de la Familia Antamina? La relación podría ser: familiar, amical, profesional, sentimental.
                    </li>
                    <li>En el caso que exista una relación entre un miembro de la Familia Antamina con la persona que ingresa o empresa que se contrata. ¿Trabajaran en la misma VP, Gerencia, Área o Proyecto?</li>
                  </ul>

                  <br>

                  Ante la duda siempre se puede recurrir a Cumplimiento para hacer una consulta.
                  <br><br>
                  <div align="center" class="embed-responsive embed-responsive-16by9">
                    <video controls preload="none" loop class="embed-responsive-item" poster="../../images/fondovideo.jpg">
                      <source src=videos/conflicto_intereses.mp4 type=video/mp4>
                    </video>
                  </div>
                  <br>
                  <div align="center" class="embed-responsive embed-responsive-16by9">
                    <video controls preload="none" loop class="embed-responsive-item" poster="../../images/fondovideo.jpg">
                      <source src=videos/antamina3.mp4 type=video/mp4>
                    </video>
                  </div>
                    <br><br>
                </div>
                <div class="contenedoder" id="divtest4" style="display:none;">
                  <h3>PREVENCIÓN DEL LAVADO DE ACTIVOS Y FINANCIAMIENTO DEL TERRORISMO</h3>
                  Por ley en el Perú existen sectores de la economía que deben implementar sistemas y controles para prevenir y detectar operaciones de lavado de activos y financiamiento del terrorismo de sus clientes. Dentro de estos sectores, denominados “Sujetos Obligados” se ha incluido a las empresas Mineras.
                  <br>
                  El lavado de activos se define como el proceso de convertir el dinero o activos ilícitos en lícitos y tiene tres modalidades:
                  <br>
                  Ejemplos:

                  <ul>
                    <li>Colocación: Cuando se introducen los fondos ilegales al sector formal

                    </li>

                    <li>Transformación: Se realizan operaciones para alejar el dinero de su fuente y cambiar su apariencia.

                    </li>
                    <li>Integración: La incorporación de los fondos en actividades económicas legítimas</li>
                  </ul>
                  <br>
                  El financiamiento del terrorismo se define como el acto de proporcionar apoyo, por cualquier medio, a terroristas u organizaciones terroristas. El apoyo puede ser a través de aportes de dinero, donaciones, entrega de bienes, entre otros.
                  <br><br>
                </div>
          <div class="contenedoder" id="divtest5" style="display:none;">
            <h3>REGALOS Y ATENCIONES</h3>
            En Antamina contamos con procedimientos para la recepción y otorgamiento de Regalos y Atenciones en línea con los estándares internacionales en materia anti-corrupción (v.g. UK Bribery Act (UKBA), Foreign Corrupt Practices Act –FCPA) que Antamina debe respetar.  Esto es aplicable tanto a aquellos regalos y atenciones ofrecidos como los recibidos por terceros ajenos a Antamina.
            <br>
            En el caso de aquellos regalos y atenciones otorgados a terceros se debe tener el cuidado necesario para evitar su indebida canalización como sobornos a funcionarios públicos y privados.
            <br>
            Con el fin de mitigar este riesgo se han establecido escalas de aprobaciones en función a los montos de los ofrecimientos.
            <br>
            <h4>Regalos y Atenciones otorgados</h4>
            <br>
            En Antamina hemos establecidos ciertos controles y limites en cuanto a la entrega de regalos y atenciones ofrecidas a terceros.  La tabla a continuación detalla las acciones y aprobaciones pertinentes.
            <br><br>
            <table border="1" cellspacing="0" cellpadding="0" width="100%" id="yui_3_9_1_2_1429652089206_120">
              <tbody id="yui_3_9_1_2_1429652089206_119"><tr id="yui_3_9_1_2_1429652089206_118">
                <td width="236" class="topcel" id="yui_3_9_1_2_1429652089206_117"><strong id="yui_3_9_1_2_1429652089206_116">Monto del Regalo Otorgado</strong></td>
                <td width="350" class="topcel"><strong>Acción</strong></td>
                </tr>
                <tr>
                  <td width="236"><p>Recuerdos menores o artículos    promocionales </p></td>
                  <td width="350"><ul>
                    <li>No requiere    informarse a Cumplimiento.</li>
                  </ul></td>
                </tr>
                <tr>
                  <td width="236"><p>Regalos menores a    US$ 150.</p></td>
                  <td width="350" valign="top"><ul>
                    <li>Requiere    aprobación del Gerente correspondiente e informar a Cumplimiento</li>
                  </ul></td>
                </tr>
                <tr>
                  <td width="236"><p>Regalos mayores a    US$ 150 y menores a US $ 1000.</p></td>
                  <td width="350" valign="top"><ul>
                    <li>Requiere    aprobación del Vicepresidente correspondiente y Vicepresidente Legal y de    Cumplimiento </li>
                    <li>Se deberá    completar la “Solicitud de aprobación para el otorgamiento de regalos y    atenciones”.</li>
                  </ul></td>
                </tr>
                <tr>
                  <td width="236"><p>Regalos mayores a    US$ 1,000. </p></td>
                  <td width="350" valign="top"><ul>
                    <li>Requiere    aprobación del Vicepresidente, Vicepresidente Legal y de Cumplimiento y    Vicepresidente de Finanzas y Administración o el CEO, </li>
                    <li>Se deberá    completar la “Solicitud de aprobación para el otorgamiento de regalos y    atenciones”.</li>
                  </ul></td>
                </tr>
              </tbody>
            </table>
            <br>
            <br>
              <table border="1" cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                  <tr>
                    <td width="236" class="topcel"><strong>Monto de la Atención Otorgada</strong></td>
                    <td width="350" class="topcel"><strong>Acción</strong></td>
                  </tr>
                  <tr>
                    <td width="236"><p>Bebidas/Alimentos ofrecidos en    reuniones de negocio en las oficinas de la Compañía.</p></td>
                    <td width="350"><ul>
                      <li>No requiere    aprobación o registro.</li>
                    </ul></td>
                  </tr>
                  <tr>
                    <td width="236"><p>Atención Igual o menor a US $100 por invitado. </p></td>
                    <td width="350"><ul>
                      <li>No requiere    aprobación, se registra en el “Registro de Regalos y Atenciones” dentro de    los 5 días de realizada la atención.</li>
                    </ul></td>
                  </tr>
                  <tr>
                    <td width="236"><p>Atención mayor a US $ 100 o por invitado. </p></td>
                    <td width="350"><ul>
                      <li>Requiere    aprobación del Vicepresidente correspondiente, Vicepresidente Legal y de    Cumplimiento, </li>
                      <li>Se deberá    completar la “Solicitud de aprobación para el otorgamiento de regalos y    atenciones” </li>
                    </ul></td>
                  </tr>
                </tbody>
              </table>
              <br><br>
              <ul>
                <li>En el caso de las atenciones la información es comunicada a Cumplimiento después de la atención y se debe remitir copia de la factura identificando a los participantes.</li>
              </ul>

              <h4>Regalos y Atenciones recibidas</h4>

              En el caso de las atenciones recibidas por terceros, no existe una obligación de reporte o difusión, pero queda a criterio de cada miembro de la familia Antamina determinar si la atención recibida esta dentro de los parámetros de usos y costumbres y que la misma no es desproporcionada o podría afectar a Antamina.  En cuanto a los regalos recibidos se debe tener el cuidado necesario para asegurarse que estos se registren de manera apropiada en la Compañía para evitar la apariencia de sobornos o favorecimientos indebidos.
              <br>
              Con el fin de mitigar el riesgo de esta situación todos los regalos recibidos valorizados por encima de US$ 150 deben ser reportados al Área de Cumplimiento vía correo electrónico.  El trabajador debe informar sobre lo que se le ha regalado, el valor estimado del regalo y que empresa / persona se lo ha entregado y la relación con el otorgante.
              No es obligatorio que los regalos de materiales promocionales o menores así como aquellos regalos que están valorizados por debajo de US$ 150 se informen a Cumplimiento, pero podemos informar a Cumplimiento si lo deseamos.
              <br>
              Adicionalmente los regalos recibidos a raíz de las relaciones  personales del trabajador (como por ejemplo de parte de su Banco, AFP, Compañía de Seguros, etc.) tampoco se informaran a Cumplimiento.
              <br><br>
              <div align="center" class="embed-responsive embed-responsive-16by9">
                <video controls preload="none" loop class="embed-responsive-item" poster="../../images/fondovideo.jpg">
                  <source src=videos/politica_regalos.mp4 type=video/mp4>
                </video>
              </div>
              <br><br>
            </div>
            <div class="contenedoder" id="divtest6" style="display:none;">
              <h3>POLÍTICAS Y PROCEDIMIENTOS</h3>

              En Antamina es importante respetar todas las políticas y procedimiento vigentes. La compañía ha establecido políticas con respecto a los procesos comerciales, autorizaciones para gastos, manejo de ofertas, rendiciones de cuenta, uso del correo electrónico y del Internet, entre otras.  Todos los empleados deben conocer y aplicar las políticas que se aplican a las operaciones de los departamentos y/o gerencias en los que laboran y que se aseguren que las mismas sean cumplidas en todo momento.

              <br>
              <h4>Discriminación y acoso </h4>

              Todos los miembros de la familia Antamina respaldarán y promoverán la política de la compañía de brindar un ambiente de trabajo en el que todos sean tratados con respeto, cuenten con igualdad de oportunidades en base a sus méritos y se mantengan libres de toda forma de discriminación y acoso.
              <br>
              En Antamina, no se tolerarán prácticas discriminatorias o cualquier forma de acoso a ningún nivel de la compañía a lo largo de la relación laboral.  Esto se aplica a temas como contratación, promoción, oportunidades de capacitación, manejo salarial, beneficios y terminación de la relación laboral.  Los miembros de la familia Antamina serán tratados por igual y sus oportunidades dependerán de sus méritos y de su capacidad para realizar el trabajo.
              <br>
              De igual manera, se deberán respetar las diferencias de edad, raza, origen, género, orientación sexual, cultura, religión, y capacidades físicas que existen entre las personas. Los miembros de la familia Antamina podrán denunciar cualquier forma de discriminación y acoso a Recursos Humanos de acuerdo a lo establecido en el presente documento y la legislación nacional aplicable.
            <br><br>
            </div>
            <div class="contenedoder" id="divtest7" style="display:none;">
              <h3>CONFIDENCIALIDAD DE LA INFORMACIÓN</h3>

              Todo integrante de la familia Antamina, por el hecho de serlo, asume una obligación de confidencialidad respecto a información reservada, sensible y/o confidencial de la compañía a la que pudiera tener acceso. En tal sentido, si manejamos u obtuviéramos información reservada, sensible y/o confidencial que no fuese de dominio público, no puede ser compartida o revelada con terceros. La única excepción es que hayamos sido previamente autorizados para hacerlo.
              <br>
              Tampoco podemos utilizar dicha información para obtener ganancias económicas personales. El mal uso de la información reservada, sensible y/o confidencial de la compañía constituye una violación al Código de Conducta y un quebrantamiento de la buena fe laboral pudiendo ocasionar la terminación del vínculo laboral e incluso el inicio de acciones penales y civiles.
            </div>
          </div><!-- /input-group -->
        </div><!-- /.col-lg-6 -->
      </div><!-- /.row -->
    </div>
  </div>
</div>
<script type="text/javascript">
$('.ann').on('click',function(){
  if($('body').width()>981){
    var altura = $('.contenedorTXT').height();
    $('.menuizq').height(altura);
    $("video").each(function(){
      $(this).get(0).pause();
    });
  }
})
</script>
@include('admin.includes.footer')
