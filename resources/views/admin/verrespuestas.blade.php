@include('admin.includes.cabecera')
@include('admin.includes.menu')
<div class="page-content">
  <div class="container" >
    <div class="conder" style="background:#fff;text-align: center;">
      <div class="row cont-pg">
        @include('admin.includes.menulateraltest')
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
          <div class="portlet light">
            <div style="text-align:right;">
              <a href="{!! url('/') !!}" style="color:#000;font-style: italic;text-decoration: underline;" title="">Volver al menú de contenidos</a>
            </div>
            <h3>RESUMEN DE INTENTO</h3>
            <div class="espaciottl"></div>
            <?php $is=1; ?>
            @foreach($examen as $nexamen)
            @foreach($preguntas as $pregunta)
            <div class="filapregunta">
              <div class="numeropreg">Pregunta {!!$is++!!}</div>
              <div class="pregunta">
                <h4 style="text-align: -webkit-left;">{!! $pregunta->pregunta !!}<span></span></h4>
                <div>
                  {!! nl2br($pregunta->contenido) !!}
                </div>
                <b></b>
                <div class="alternativas">
                  <ul>
                    <?php
                    $alternativas = App\Commands\Funciones::creaPreguntas($pregunta->id_pregunta);
                    foreach ($alternativas as $prg) { ?>
                      <li <?php if($prg->verasidad==$pregunta->valides && $prg->id==$pregunta->id_alternativa){ echo 'style="background:#E99130;"'; }?>>
                        @if($prg->verasidad == 1)
                        {!! $prg->respuesta !!} <b>Correcta</b>
                        @else
                        {!! $prg->respuesta !!}
                        @endif
                      </li>
                      <?php
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
            @endforeach
            @endforeach
          </div>
        </div>
      </div>
      <!-- END PAGE CONTENT INNER -->
    </div>
  </div>
</div>
@include('admin.includes.footer')
