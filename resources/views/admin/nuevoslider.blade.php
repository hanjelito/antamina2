@include('admin.includes.cabecera')
@include('admin.includes.menu')
	<div class="page-head">
	    <div class="container">
	      <!-- BEGIN PAGE TITLE -->
	      <div class="page-title">
	        <h1>Nuevo Slider</h1>
	      </div>
	      <!-- END PAGE TITLE -->
	      <!-- BEGIN PAGE TOOLBAR -->
	      
	      <!-- END PAGE TOOLBAR -->
	    </div>
    </div>
	

	<div class="page-content">
		<div class="container">
		
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-6">
					
				
	                    <div class="portlet box yellow">
	                        <div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gift"></i>Sliders
								</div>
							</div>
							<div class="portlet-body form">
								{!! Form::open(array('url' => 'admin/addslider','files' => true)) !!}
    							<div class="form-group">
    								{!! Form::label('nombre','Nombre:') !!}
    								{!! Form::text('nombre',null,['class'=> 'form-control col-md-offset-2'])!!}
    								<div class="space" style="height: 20px!important; clear: both!important;"></div>
    								{!! Form::label('imagen','Imagen:') !!}
    								{!! Form::file('imagen',['class'=> 'col-md-offset-2'])!!}
    								<div class="space" style="height: 20px!important; clear: both!important;"></div>	
    								{!! Form::label('titulo','Titulo:') !!}
    								{!! Form::text('titulo',null,['class'=> 'form-control col-md-offset-2'])!!}
    								<div class="space" style="height: 20px!important; clear: both!important;"></div>	
    								{!! Form::label('descripcion','Descripción:') !!}
    								{!! Form::text('descripcion',null,['class'=> 'form-control col-md-offset-2'])!!}
    								<div class="space" style="height: 20px!important; clear: both!important;"></div>	
    								{!! Form::label('url','Url:') !!}
    								{!! Form::text('url',null,['class'=> 'form-control col-md-offset-2'])!!}
    								<div class="space" style="height: 20px!important; clear: both!important;"></div>	
    								
    							</div>
    							<div class="form-actions fluid">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" class="btn green">Guardar Slider</button>
											</div>
										</div>
									</div>
								{!! Form::close() !!}

							</div>
	                    </div>
							           
                

				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>


@include('admin.includes.footer')