@include('admin.includes.cabecera')
@include('admin.includes.menu')
<div class="page-content">
  <div class="container" >
    <div class="conder" style="background:#fff;text-align: center;">
      <div class="row cont-pg">
		  @include('admin.includes.menulateraltest')
		  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
				<div class="portlet light">
				  	<div style="text-align:right;">
		          <a href="{!! url('/') !!}" style="color:#000;font-style: italic;text-decoration: underline;" title="">Volver al menú de contenidos</a>
		         </div>
			    	<div class="input-group">
				    	<div class="contenedoder" id="">
					      	<h3>INDICACIONES</h3>
							<ul>
								<li>El test consta de 20 preguntas aleatorias de olpción múltiple las cuales tienen un puntaje de 01.</li>
								<li>La nota para aprobar la evalucación es 15.</li>
								<li>Puedes desarrollar el test la veces que quieras</li>
								<li>Se tomará en cuenta la nota más alta de tu calificación.</li>
							</ul>
						</div>
			    	</div><!-- /input-group -->
		    	</div>
		  </div><!-- /.col-lg-6 -->
		</div><!-- /.row -->
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#indicaciones').attr('class','activo');
	});
</script>
@include('admin.includes.footer')
