@include('admin.includes.cabecera')
@include('admin.includes.menu')
<div class="page-content">
  <div class="container" >
    <div class="conder" style="background:#fff;text-align: center;">
		<div class="row cont-pg">
		  @include('admin.includes.menulateral')
		  <div class="col-md-9">
		  		<!-- BEGIN PAGE TITLE -->
		      <div class="col-md-9">
		        <h1>Examenes Registrados</h1>
		      </div>
		      <!-- END PAGE TITLE -->
		      <!-- BEGIN PAGE TOOLBAR -->
		      <div class="col-md-3">
		 	 	<div class="btn-theme-panel">
		            <a href="#myModal" role="button" class="btn	" data-toggle="modal">
		             	<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
		            </a>
		        </div>
		      </div>
		      <!-- END PAGE TOOLBAR -->
				<div class="portlet light">

					<div class="portlet-body">
					  <div class="table-scrollable">
					    <table class="table table-striped table-hover">
					    <thead>
					    <tr>
					      <th>
					         #
					      </th>
					      <th>
					         Nombre Examen
					      </th>

					      <th>
					         Editar
					      </th>
					       <th>
					         Eliminar
					      </th>

					    </tr>
					    </thead>
					    <tbody>
					    	@foreach ($dtexamen as $examenes)
					      	<tr>
					          <td>
					             {{ $examenes->id }}
					          </td>
					          <td>
					             {{ $examenes->titulo }}
					          </td>

					          <td>
					            <a href="<?=URL::to('/admin/editexamen/'.$examenes->id); ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
					          </td>

					          <td>
					            <a href="<?=URL::to('/admin/editexamen/'.$examenes->id); ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
					          </td>
					    	</tr>
					    	@endforeach
					    </tbody>

					    </table>
					  </div>
					</div>
				</div>
		    </div>
		</div>
			<!-- END PAGE CONTENT INNER -->
	</div>
  </div>
</div>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
    	{!! Form::open(array('url' => 'admin/saveexamen/','files' => true)) !!}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Agregar Examen</h4>
            </div>
            <div class="modal-body">
                <div class="tituloprgt">
                	<label for="">Nombre de Examen: </label>
                	<input type="text" class="form-control" name="nameexamen" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar Cambios</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@include('admin.includes.footer')
