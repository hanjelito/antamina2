@include('admin.includes.cabecera')
@include('admin.includes.menu')
<script type="text/javascript">
$(function(){

  $('#counter_2').countdown({
    image: '../../images/digits.png',
    startTime: '60:00',
    timerEnd: function(){
      $( "#enviar" ).trigger( "click" );

    },
    format: 'mm:ss'
  });
});
$( document ).ready(function() {
  $('.resumir').click(function(){
    var restantes = $('.alternativas').length - $('input:radio:checked').length;
    /*$(this).find('.misradiobutons').each(function(){

    if( $(this).prop('chekend')==true){
    var salida = 1
  }
})*/
$('.restantes').html("Quedan " +restantes.toString()+" preguntas sin responder")

});
});
</script>
@foreach ($dtexamen as $examenes)
<div class="page-content">
  <div class="container" >
    <div  class="conder" style="background:#fff;text-align: center;">
      <div class="row cont-pg">
        @include('admin.includes.menutest')
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
          {!! Form::open(array('url' => 'admin/createst/'.$examenes->id,'files' => true)) !!}
          <div class="portlet light">
            <div style="text-align:right;">
              <a href="{!! url('/') !!}" style="color:#000;font-style: italic;text-decoration: underline;" title="">Volver al menú de contenidos</a>
            </div>
            <h3>RESPONDER TODAS LAS PREGUNTAS</h3>
            <div class="espaciottl"></div>
            {!! Form::text('idtest',$idtestexam,['class'=> ' form-control col-md-offset-2 invi'])!!}
            <div class="portlet-body">
              <div class="table-scrollable">
                <?php
                $preguntas = App\Commands\Funciones::creaExamen($examenes->id);
                $cont =1;
                foreach ($preguntas as $key) {
                  ?>
                  <div class="filapregunta">
                    <div class="numeropreg">Pregunta {!! $cont !!}</div>
                    <div class="pregunta">
                      <h4 style="text-align: -webkit-left;">{!!$key->pregunta!!}</h4>
                      <div>
                        {!! nl2br($key->contenido) !!}
                      </div>
                      <div class="alternativas">
                        <ul>
                          <?php
                          $alternativas = App\Commands\Funciones::creaPreguntas($key->id);
                          foreach ($alternativas as $prg) { ?>
                            <li>
                              <input type="radio" name="prd{!! $prg->id_pregunta !!}" value="{!! $prg->id !!}" placeholder="">
                              {!! $prg->respuesta !!}
                            </li>
                            <?php
                          }
                          ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <?php
                  $cont = $cont +1;
                }
                ?>
              </div>

              <!--<button type="submit" class="btn green right">Enviar</button>-->
            </div>
          </div>
          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel"> CONFIRMACIÓN</h4>
                </div>
                <div class="modal-body">
                  <div class="page-content">
                    <div class="">
                      <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="">
                          <div class="portlet box yellow">
                            <div class="form-actions fluid">
                              <div class="row">
                                <div class="text-center restantes">
                                </div>
                              </br>
                              <div class="col-md-offset-0 col-md-3">
                                <button type="button" class="btn orange2" data-dismiss="modal" aria-label="Close">Continuar</button>
                              </div>
                              <div class="col-md-offset-4 col-md-4">
                                <button id="enviar" type="submit" class="btn orange2">Finalizar</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                  </div>
                  <!-- END PAGE CONTENT INNER -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {!! Form::close() !!}
      <button class="btn orange2 right resumir" data-toggle="modal" data-target="#myModal">Finalizar evaluación</button>
      <div class="espacioinf">
      </div>
    </div>
  </div>
  <!-- END PAGE CONTENT INNER -->
</div>
</div>
</div>
@endforeach


<script>
function get_time_difference(earlierDate, laterDate){
  var nTotalDiff = laterDate.getTime() - earlierDate.getTime();
  var oDiff = new Object();

  oDiff.days = Math.floor(nTotalDiff/1000/60/60/24);
  nTotalDiff -= oDiff.days*1000*60*60*24;

  oDiff.hours = Math.floor(nTotalDiff/1000/60/60);
  nTotalDiff -= oDiff.hours*1000*60*60;

  oDiff.minutes = Math.floor(nTotalDiff/1000/60);
  nTotalDiff -= oDiff.minutes*1000*60;

  oDiff.seconds = Math.floor(nTotalDiff/1000);
  return oDiff;

}
function use_time_difference(){
  finish = new Date(2013, 02, 15, 11, 30, 00);
  dateCurrent = new Date();
  oDiff = get_time_difference(dateCurrent, finish);
  oDiff.days<=9?days = '0'+oDiff.days: days = oDiff.days;
  oDiff.hours<=9?hours = '0'+oDiff.hours: hours = oDiff.hours;
  oDiff.minutes<=9?minutes = '0'+oDiff.minutes: minutes = oDiff.minutes;
  oDiff.seconds<=9?seconds = '0'+oDiff.seconds: seconds = oDiff.seconds;
  time = days + ":" + hours + ":" + minutes + ":" + seconds;
}
use_time_difference();
$('#counter').countdown({
  stepTime: 60,
  format: 'dd:hh:mm:ss',
  startTime: time,
  digitImages: 6,
  digitWidth: 53,
  digitHeight: 77,
  image: "../../images/digits.png"
});
</script>


@include('admin.includes.footer')
