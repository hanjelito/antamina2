@include('admin.includes.cabecera')
@include('admin.includes.menu')
<div class="page-content">
	<div class="container" >
		<div class="" style="background:#fff;text-align: center;">
			<div class="row cont-pg">
				<div class="col-md-12">
					<div class="portlet light">
						<div class="noPrint" style="text-align:right;">
		          <a href="{!! url('/') !!}" style="color:#000;font-style: italic;text-decoration: underline;" title="">Volver al menú de contenidos</a>
		         </div>
						<div class="espcert">

						</div>
						<div class="miPrint" style="text-align:center;">
							<img src="../../images/imgtop.jpg" alt="" />
							<div style="text-align:">
			        <h3 >PLATAFORMA DE CAPACITACIÓN FAMILIA ANTAMINA 2015</h3>
			      </div>
						</div>
						<div class="portlet-body">
							<h3 class="centrado ttlocer">CERTIFICADO DE PARTICIPACIÓN</h3>
							<div class="table-scrollable">
								<div class="table-responsive">
									<table class="table table-bordered tbls">
										<thead>
											<tr>
												<th>NOMBRE</th>
												<th>FECHA DE EVALUACIÓN</th>
												<th>NOTA</th>
											</tr>
										</thead>
										<tbody>
											<?php $impnota=0; ?>
											@foreach ($user as $useDB)
											<tr>
												<td>{{ $useDB->name }} {{ $useDB->second_name }} {{ $useDB->last_name }} {{ $useDB->last_namem }}</td>
												<?php
													$rest = App\Commands\Funciones::NotafechaMaxima($useDB->fech);
													$fechasn = explode(' ', $rest[1]);
												?>
												<td>{{ App\Commands\Funciones::InverFecha($fechasn[0]) }}</td>
												<td>{{ $rest[0] }}</td>
												<?php $impnota=$rest[0]; ?>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<div class="agradecimientos centrado">
									¡Gracias por participar!
								</div>
							</br>
						</div>
						<div>
							Mediante el presente se certifica que has completado satisfactoriamente el Curso en línea sobre el Código de Conducta de Compañía Minera Antamina para este periodo.
						</div>
						<div class="condicion noPrint">
							Recuerda que solo podrás descargar el certificado si tu calificación es igual o mayor a 15.
						</div>
					</div>
					@if($impnota>14)
					<div style="text-align:right;">
						<a href="#	" onclick="javascript:window.print();" class="btn yellow orange2 noPrint" title="">IMPRIMIR</a>
					</div>
					@endif
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
</div>
@include('admin.includes.footer')
