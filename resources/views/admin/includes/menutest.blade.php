<script src="<?php echo URL::asset('timer/jquery.downCount.js'); ?>"> </script>
<div class="col-md-3 menuizq">
	<div class="col-md-12 col-sm-12 col-xs-12" align="center">
		<p class="text-center timer-titu"><strong>  TIEMPO RESTANTE</strong></p>
		<div class="countdown timer-hora">
			<p style="display:inline" class="hours">00</p>
			<span>:</span>
			<p style="display:inline" class="minutes">00</p>
			<span>:</span>
			<p style="display:inline" class="seconds">00</p>							
		</div>
	</div>
</div><!-- /.col-lg-6 -->

<script class="source" type="text/javascript">
	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var hora =  d.getHours()+1;
	var mins = d.getMinutes();
	var sec = d.getSeconds();
	var output = (month<10 ? '0' : '') + month  + '/' + (day<10 ? '0' : '') + day +  '/' +  d.getFullYear() +  ' ' + hora+':'+mins+':'+sec;
	$('.countdown').downCount({
		date: output,
		offset: -5
	}, function () {
		$( "#enviar" ).trigger( "click" );
	});
</script>

@yield('content')
