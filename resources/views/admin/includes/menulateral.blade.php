<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 menuizq">
  <div class="menuslat">
    <h4>MENÚ DE CONTENIDOS</h4>
  <ul>
    <li><a class="ann" id="a1" onclick="cargar('.contenedoder','#divtest1','#a1','.ann')" style="color: rgb(215, 140, 32);" >Presentación</a></li>
    <li><a class="ann" id="a2" onclick="cargar('.contenedoder','#divtest2','#a2','.ann')" >Soborno</a></li>
    <li><a class="ann" id="a3" onclick="cargar('.contenedoder','#divtest3','#a3','.ann')">Conflicto de Interés</a></li>
    <li><a class="ann" id="a4" onclick="cargar('.contenedoder','#divtest4','#a4','.ann')">Prevención del Lavado de Activos y Financiamiento del Terrorismo</a></li>
    <li><a class="ann" id="a5" onclick="cargar('.contenedoder','#divtest5','#a5','.ann')">Regalos y Atenciones</a></li>
    <li><a class="ann" id="a6" onclick="cargar('.contenedoder','#divtest6','#a6','.ann')">Políticas y Procedimientos</a></li>
    <li><a class="ann" id="a7" onclick="cargar('.contenedoder','#divtest7','#a7','.ann')">Confidencialidad de la Informacion</a></li>
  </ul>
  <a href="<?= url('admin/indicaciones'); ?>" class="btn yellow orange" title="">TEST EN LINEA</a>

  </div><!-- /input-group -->
</div><!-- /.col-lg-6 -->
  @yield('content')
