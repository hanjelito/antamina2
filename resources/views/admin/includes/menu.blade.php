<div class="page-content">
    <nav class="navbar navbar-default headvar">
      <div class="container" >
      <div class="col-xs-3 imglogo">
        <a href="{{ url('/') }}"><img src="../../images/logotop.jpg" alt=""></a>
      </div>
      <div class="container col-xs-9 col-sm-12 col-md-12 col-lg-12" >
        <div class="navbar-header usuarios1">
          <a class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            Hola: {{ Auth::user()->name }}
          </a>
        </div>
        <div class="collapse navbar-collapse menussp" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav imglogo2">
            <li><a href="{{ url('/') }}" style="display: inline;"><img src="../../images/logotop.jpg" alt=""></a></li>
           <!-- <li><a href="{{ url('/admin/examenes') }}">Crear Examene</a></li>-->
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="" aria-expanded="false">
                <div class="usuarios">
                  Hola: {{ Auth::user()->name }} <span class="caret"></span>
                </div>
                <div class="menusdato">
                  MENÚ <span class="caret"></span>
                </div>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ url('/admin/test') }}">Tabla de evaluaciones</a></li>
                <li><a href="{{ url('/admin/actualizarpass') }}">Actualizar contraseña</a></li>
                @if (Auth::user()->nivel == 2)
                  <li><a href="{{ url('/superadmin') }}">Admin</a></li>
                @endif
                <li class="divider"></li>
                <li><a href="{{ url('/auth/logout') }}">Salir</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
    </nav>
    <div class="page-content">
      <div class="container" >
          <div class="row cont-pg titulo noPrint">
            <div style="text-align:">
            <h3 >PLATAFORMA DE CAPACITACIÓN FAMILIA ANTAMINA 2015</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="page-content">
      <div class="container">
        <div class="row cont-pg margen noPrint">
          <img src="../../images/banner.jpg" class="img-responsive">
        </div>
      </div>
    </div>
</div>
  @yield('content')
