@include('admin.includes.cabecera')
@include('admin.includes.menu')
@foreach ($dtexamen as $examenes)
<div class="page-content">
  <div class="container" >
    <div class="conder" style="background:#fff;text-align: center;">
      <div class="row cont-pg">
		  @include('admin.includes.menulateral')
		  <div class="col-md-9">
	          		 <!-- BEGIN PAGE TITLE -->
		      <div class="col-md-9">
		        <h1>{{ $examenes->titulo }}</h1>
		      </div>
		      <!-- END PAGE TITLE -->
		      <!-- BEGIN PAGE TOOLBAR -->
		      <div class="col-md-3">
		 	 	<div class="btn-theme-panel">
		 	 		<a href="#myModal" role="button" class="btn	" data-toggle="modal"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span></a>
		        </div>
		      </div>
	      <!-- END PAGE TOOLBAR -->
		          <div class="portlet light">

		            <div class="portlet-body">
		              <div class="table-scrollable">
		                <?php
		                $preguntas = App\Commands\Funciones::creaExamenEdit($examenes->id);
		                $contadorPrg=1;
		                foreach ($preguntas as $key) {
		                	?>
		                		<div class="titulo">
		                			<h3>{!! $contadorPrg++; !!}.- {!!$key->pregunta!!} <span>({!! $key->puntaje !!}) Pts.</span></h3>
		                			<div>
		                				{!! nl2br($key->contenido) !!}
		                			</div>
		                		</div>
		                		<div class="alternativas">
		                			<ul>
		                			<?php
		                				$alternativas = App\Commands\Funciones::creaPreguntas($key->id);
		                				$contadorAlt=1;
		                				foreach ($alternativas as $prg) { ?>
		                				<li>
		                					{!! $contadorAlt++; !!}.-
		                					{!! $prg->respuesta !!}
		                					<?php if($prg->verasidad==1)
		                					{
		                						echo '<b>Correcta</b>';
		                					} ?>
		                				</li>
		                				<?php
						                }
						            ?>
		                			</ul>
		                		</div>
		                	<?php
		                }
		                ?>
		              </div>
		            </div>
		          </div>

		        </div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
</div>
	<div id="myModal" class="modal fade">
    <div class="modal-dialog">
    	{!! Form::open(array('url' => 'admin/genpregunta/'.$examenes->id,'files' => true)) !!}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Agregar Pregunta</h4>
            </div>
            <div class="modal-body">
                <div class="tituloprgt">
                	<label for="">Pregunta: </label>
                	<input type="text" class="form-control" name="ttlprgta" value="">
                </div>
                <div class="tituloprgt">
                	<label for="">Puntos: </label>
                	<input type="text" class="form-control" name="pntg" value="">
                </div>
                <div class="contenido">
                	<label for="">Contenido: </label>
                	<textarea name="ctndoprgta" class="form-control textarea"></textarea>
                </div>
                <div>
                	<a href="javascript:;" class="addpr" title="">
                		<label for="">Agregar Alternativas: </label>
                		<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                	</a>
                </div>
                <div class="altrnvs">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar Cambios</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endforeach
<div id="altche" style="display:none;">
	<div id="addalt" >
		<input type="text" class="form-control" name="prgtas[]" value="" style="display: inline;width: 65%;">
		<input type="radio" class="prgvrddra" name="check">
		<input type="hidden" class="veridico" name="veridico[]" value="0">
		<a href="javascript:;" onclick="elimianAlt(this)" title=""><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span></a>
	</div>
</div>

<script type="text/javascript">
	$('.addpr').on('click', function(event) {
		var contnnd = $('#altche #addalt').clone();
		$('.veridico').val(0);
		$('.prgvrddra').prop('checked', false);
		$('.altrnvs').append(contnnd);
		$('.prgvrddra').on('click', function(event) {
			$('.veridico').val(0);
			$(this).parent().find('.veridico').val(1);
		});
	});

	function elimianAlt(e){
		$(e).parent().remove();
	}
	elimianAlt();
</script>
@include('admin.includes.footer')
