@include('admin.includes.cabecera')
@include('admin.includes.menu')
<div class="page-content">
  <div class="container" >
    <div class="conder" style="background:#fff;text-align: center;">
      <div class="row cont-pg">
        @include('admin.includes.menulateraltest')
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
          <div class="portlet light">
            <div style="text-align:right;">
              <a href="{!! url('/') !!}" style="color:#000;font-style: italic;text-decoration: underline;" title="">Volver al menú de contenidos</a>
            </div>
            <div class="portlet-body">
              <div class="table-scrollable contenedoder">
                <h3>RESUMEN DE INTENTOS PREVIOS</h3>
                <div class="espaciottl">

                </div>
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>INTENTO</th>
                        <th>FECHA</th>
                        <th>HORA</th>
                        <th>CALIFICACIÓN</th>
                        <th>REVISIÓN</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $int = 1; ?>
                      @foreach ($result as $intento)
                      <tr>
                        <td>{{ $int++ }}</td>
                        <td>
                          <?php $intentofh=explode(' ', $intento->created_at) ?>
                          {{ App\Commands\Funciones::InverFecha($intentofh[0]) }}
                        </td>
                        <td>{{ $intentofh[1] }}</td>
                        <td class="puntjs">{{ $intento->puntaje }}</td>
                        <td class="urls"><a href="{!! URL('admin/respuestas/'.$intento->id) !!}" title="">VER</a></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- END PAGE CONTENT INNER -->
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#intentos').attr('class','activo');
});
</script>
@include('admin.includes.footer')
