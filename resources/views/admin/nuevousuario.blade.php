@include('admin.includes.cabecera')
@include('admin.includes.menu')
<link href='<?php echo URL::asset('css/nuevouser.css'); ?>' rel='stylesheet' type='text/css'>
<div class="page-content">
	<div class="container" >
		<div class="conder" style="background:#fff;text-align: center;">
			<div class="row cont-pg">
				<div class="col-md-12">
					<div class="portlet light">
						<div style="text-align:right;">
							<a href="{!! url('/') !!}" style="color:#000;font-style: italic;text-decoration: underline;" title="">Volver al menú de contenidos</a>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-sm-offset-1 col-xs-offset-1 col-xs-10 col-sm-10 col-md-7">
							{!! Form::open(array('url' => 'admin/savenuevousuario')) !!}
    							<div class="form-group">
    								{!! Form::label('codigo','Código:') !!}
    								{!! Form::text('codigo',null,['class'=> 'form-control','maxlength' => '4'])!!}
    							</div>
                                <div class="form-group">
                                    {!! Form::label('name','Nombre:') !!}
                                    {!! Form::text('name',null,['class'=> 'form-control'])!!}
                                </div>
    							<div class="form-group">
    								{!! Form::label('last_name','Apellidos:') !!}
    								{!! Form::text('last_name',null,['class'=> 'form-control'])!!}
    							</div>
    							<div class="form-group">
    								{!! Form::label('genero','Género:') !!}
    								{!! Form::text('genero',null,['class'=> 'form-control'])!!}
    							</div>
                                <div class="form-group">
                                    {!! Form::label('email','Email:') !!}
                                    {!! Form::text('email',null,['class'=> 'form-control','placeholder' => 'ejemplo@familiaantamina.com'])!!}
                                </div>
    							<div class="form-group">
    								{!! Form::label('puesto','Puesto:') !!}
    								{!! Form::text('puesto',null,['class'=> 'form-control'])!!}
    								<!--<div class="space" style="height: 20px!important; clear: both!important;"></div>-->
    							</div>
    							<div class="form-group">
    								{!! Form::label('area','Área:') !!}
    								{!! Form::text('area',null,['class'=> 'form-control'])!!}
    							</div>
    							<div class="form-group">
    								{!! Form::label('gerencia','Gerencia:') !!}
    								{!! Form::text('gerencia',null,['class'=> 'form-control'])!!}
    							</div>
    							<div class="form-group">
    								{!! Form::label('categoria','Categoría:') !!}
    								{!! Form::text('categoria',null,['class'=> 'form-control'])!!}
    							</div>
    							<div class="form-group">
    								{!! Form::label('nivel','Nivel:') !!}
    								{!! Form::select('nivel', array('' => 'Selecione el nivel') + $niveles,null,['class'=> 'form-control'])!!}
    							</div>
    							<div class="form-actions fluid">
									<div class="row">
										<div class="text-center">
											<button type="submit" class="btn" style="background:#E99130;color:white;">Guardar Usuario</button>
										</div>
									</div>
								</div>
							{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
</div>
@include('admin.includes.footer')
